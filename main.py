from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def load_data(start_date, finish_date=datetime.now().strftime('%d.%m.%Y')):
    """
    Функция, загружающая данные с даты №1 по дату №2
    :param start_date: дата начала
    :param finish_date: дата завершения
    :return tables: dataframe из модуля pandas с сохраненной таблицей
    """
    full_url = f'http://cbr.ru/statistics/flikvid/?UniDbQuery.Posted=True&UniDbQuery.From={start_date}&UniDbQuery.To={finish_date}'
    tables, = pd.read_html(full_url, encoding='utf-8', thousands="ª", decimal="ª", header=1)
    tables.to_csv('tables.csv', index=False)
    return tables


def weekdays(date_list):
    """
    Функция, заносящая в список дни недели на русском языке по их идентификатору из модуля datetime
    :param date_list: список дней недели в виде идентификаторов из модуля datetime
    :return date_list: список дней недели в соответствии с параметром date_list
    """
    days = {1: "Понедельник",
                2: "Вторник",
                3: "Среда",
                4: "Четверг",
                5: "Пятница",
                6: "Суббота",
                7: "Воскресенье"}
    for key, value in enumerate(date_list):
        date_list[key] = days[int(date_list[key])]
    return date_list


def draw_plot(label_y, label_x, date_list, values_list, key, time_plot):
    """
    Функция для построения и сохранения изображения с графиком
    :param label_y: подпись оси y
    :param label_x: подпись оси x
    :param date_list: список дат для оси x
    :param values_list: список значений для оси y
    :param key: ключ для наименования изображения с графиком
    :param time_plot: папка для хранения изображения с графиком
    :return:
    """
    plt.figure(figsize=(25, 9))
    plt.plot(date_list, values_list)
    plt.ylabel(label_y)
    plt.xlabel(label_x)
    plt.grid(True)
    plt.scatter(date_list, values_list)
    plt.savefig(f'plots/{time_plot}/plot{key}')
    plt.close()


def draw_diff_plot(label_y, label_x, date_list, value_list, key, time_plot):
    """
    Функция для построения и сохранения графика, соответствующего информации о разнице среднего скользяшего и значений
    :param date_list: список дат для оси x
    :param value_list: список значений для оси y
    :param key: ключ для наименования изображения с графиком
    :return:
    """
    x = np.array(date_list)
    y = np.array(value_list)
    plt.figure(figsize=(25, 9))
    plt.plot(x, y, color='blue')
    plt.ylabel(label_y)
    plt.xlabel(label_x)
    plt.grid(True)
    plt.scatter(x, y, color='blue')

    all_sum = 0
    for i in value_list:
        all_sum += i

    middle_number = all_sum / 20
    plt.fill_between(x, y, middle_number, where=(y < middle_number), color='red', interpolate=True, alpha=0.3)
    plt.text(1, 430, f'Среднее скользящее - {middle_number}')
    plt.savefig(f'plots/{time_plot}/plot{key}')
    plt.close()


def weekly_plot(table):
    """
    Функция, считающая информацию со среды по вторник, или же со среды и до среды, если информации по вторнику нет
    :param table: data-frame библиотеки pandas
    :return:
    """
    values_list = []
    date_list = []
    marker = False
    for key, value in enumerate(table['Дата']):
        date = datetime.strptime(value, '%d.%m.%Y')
        if datetime.isoweekday(date) == 3 and marker is False:
            marker = True
        elif datetime.isoweekday(date) == 3 and marker:
            if not values_list:
                continue
            marker = False
            new_date_list = weekdays(date_list)
            draw_plot('Изменение наличных денег в обращении', 'День недели', new_date_list, values_list, key, 'weekly')
            new_date_list.clear()
            values_list.clear()
            date_list.clear()
            continue
        if marker:
            number_value = table['Изменение наличных денег в обращении (вне Банка России)'][key].replace(',', '.')
            number_value = number_value.replace(' ', '')
            number_value = float(number_value)
            values_list.append(number_value)
            date_list.append(str(datetime.isoweekday(date)))
        if datetime.isoweekday(date) == 2 and marker:
            if not values_list:
                continue
            marker = False
            new_date_list = weekdays(date_list)
            draw_plot('Изменение наличных денег в обращении', 'День недели', new_date_list, values_list, key, 'weekly')
            new_date_list.clear()
            values_list.clear()
            date_list.clear()


def monthly_plot(table):
    """
    Функция, считающая информацию с первого числа каждого месяца до 1 числа следующего месяца
    :param table: data-frame библиотеки pandas
    :return:
    """
    values_list = []
    date_list = []
    for key, value in enumerate(table['Дата']):
        date = datetime.strptime(value, '%d.%m.%Y')
        tt = date.timetuple()
        if tt[2] == 1:
            draw_plot('Изменение остатков средств на счетах расширенного правительства', 'Дата', date_list, values_list,
                      key, 'monthly')
            values_list.clear()
            date_list.clear()
        number_value = \
            table['Изменение остатков средств на счетах расширенного правительства в Банке России и прочие операции'][
                key].replace(',', '.')
        number_value = number_value.replace(' ', '')
        number_value = float(number_value)
        values_list.append(number_value)
        date_list.append(f'{tt[2]}.{tt[1]}')


def middle_twenty_plot(table):
    """
    Функция, считающая разницу между средним скользящим и значением из таблицы
    :param table: data-frame библиотеки pandas
    :return:
    """
    values_list = []
    date_list = []
    day_marker = 0
    for key, value in enumerate(table['Дата']):
        date = datetime.strptime(value, '%d.%m.%Y')
        tt = date.timetuple()
        if day_marker == 20:
            draw_diff_plot('Изменение остатков средств на счетах расширенного правительства', 'Дата', date_list,
                           values_list, key, 'twenty')
            values_list.clear()
            date_list.clear()
            day_marker = 0
        number_value = \
            table['Изменение остатков средств на счетах расширенного правительства в Банке России и прочие операции'][
                key].replace(',', '.')
        number_value = number_value.replace(' ', '')
        number_value = float(number_value)
        values_list.append(number_value)
        date_list.append(f'{tt[2]}.{tt[1]}')
        day_marker += 1


def monthly_plot_diff(table):
    """
    Функция, считающая информацию с первого числа каждого месяца до 1 числа следующего месяца, и находящая
    среднее скользящее
    :param table: data-frame библиотеки pandas
    :return:
    """
    values_list = []
    date_list = []
    for key, value in enumerate(table['Дата']):
        date = datetime.strptime(value, '%d.%m.%Y')
        tt = date.timetuple()
        if tt[2] == 1:
            draw_diff_plot('Изменение наличных денег в обращении (вне Банка России)', 'Дата', date_list, values_list,
                      key, 'monthly_diff')
            values_list.clear()
            date_list.clear()
        number_value = \
            table['Изменение наличных денег в обращении (вне Банка России)'][
                key].replace(',', '.')
        number_value = number_value.replace(' ', '')
        number_value = float(number_value)
        values_list.append(number_value)
        date_list.append(f'{tt[2]}.{tt[1]}')


table = load_data('01.01.2022')
table.sort_index(ascending=False, inplace=True)
weekly_plot(table)
monthly_plot(table)
middle_twenty_plot(table)
monthly_plot_diff(table)
